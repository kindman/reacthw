"use strict";

const Item = ({ item, imgUrl }) => {
  const price = (price, currency_code) => {
    if (currency_code === "USD") {
      return "$" + price;
    } else if (currency_code === "EUR") {
      return "€" + price;
    } else {
      return price.toLocaleString("it-IT", {
        style: "currency",
        currency: currency_code
      });
    }
  };

  const titleParsing = title => {
    if (typeof title !== "string") return "";
    if (item.title.length > 50) {
      return item.title.slice(0, 50) + "...";
    }
    return item.title;
  };

  const quantityLevel = quantity => {
    if (typeof quantity !== "number") return "";
    if (quantity <= 10) return "level-low";
    if (quantity <= 20) return "level-medium";
    return "level-high";
  };
  return (
    <div className="item">
      <div className="item-image">
        <a href={item.url}>
          <img src={imgUrl} />
        </a>
      </div>
      <div className="item-details">
        <p className="item-title">{titleParsing(item.title)}</p>
        <p className="item-price">{price(item.price, item.currency_code)}</p>
        <p className={`item-quantity ${quantityLevel(item.quantity)}`}>
          {item.quantity}
        </p>
      </div>
    </div>
  );
};

const Listing = ({ items }) => {
  return (
    <div className="item-list">
      {items.map(el => {
        const item = {
          url: el.url,
          title: el.title,
          currency_code: el.currency_code,
          price: el.price,
          quantity: el.quantity
        };
        return (
          <Item
            item={item}
            imgUrl={el.MainImage.url_570xN}
            key={el.listing_id}
          />
        );
      })}
    </div>
  );
};

Listing.defaultProps = {
  items: []
};

fetch("https://neto-api.herokuapp.com/etsy")
  .then(res => {
    if (200 <= res.status && res.status < 300) {
      document.querySelector(".loading").style.display = "none";
      return res;
    }
    throw new Error(res.statusText);
  })
  .then(res => res.json())
  .then(res => {
    ReactDOM.render(<Listing items={res} />, document.getElementById("root"));
  })
  .catch(err => console.log(err));
