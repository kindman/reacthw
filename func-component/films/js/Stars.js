"use strict";

function Stars({ count }) {
  const starsCounter = new Array(count).fill().map((item,id) => (
    <li key={id}>
      <Star />
    </li>
  ));
  return (
    <ul className="card-body-stars u-clearfix">
     {starsCounter}
    </ul>
  );
}
