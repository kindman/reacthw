'use strict';

const MessageHistory = ({list}) => {
    const messagesList = list.map(el => {
        if(el.type === 'response') {
            return <Response key={el.id} from={el.from} message={el} />
        } else if(el.type === 'message') {
            return <Message key={el.id} from={el.from} message={el} />
        } else if(el.type === 'typing') {
            return <Typing key={el.id} from={el.from} message={el} />
        }
    })
    return (
        <ul>
            {messagesList}
        </ul>
    )
}

MessageHistory.defaultProps = {
    list: []
}