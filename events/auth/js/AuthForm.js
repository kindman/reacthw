'use strict';

const AuthForm = ({ onAuth }) => {
    function authentication(event) {
        event.preventDefault();
        if ( (!onAuth) || (typeof onAuth !== 'function') ) return null;

        const form = event.currentTarget.elements;
        const account = {
            name: form.name.value,
            email: form.email.value,
            password: form.password.value
        };

        onAuth(account);
    }

    function checkEmail(event) {
        event.currentTarget.value = event.currentTarget.value
            .replace(/[^\w@\.-]+/gi, "");
    }
    
    function checkPassword(event) {
        event.currentTarget.value = event.currentTarget.value
            .replace(/[^\w]+/gi, "");
    }

    return (
        <form className="ModalForm" onSubmit={authentication} action="/404/auth/" method="POST">
            <div className="Input">
                <input required name="name" type="text" placeholder="Имя" />
                <label></label>
            </div>
            <div className="Input">
                <input onChange={checkEmail} name="email" type="email" placeholder="Электронная почта" />
                <label></label>
            </div>
            <div className="Input">
                <input onChange={checkPassword} required name="password" type="password" placeholder="Пароль" />
                <label></label>
            </div>
            <button type="submit">
                <span>Войти</span>
                <i className="fa fa-fw fa-chevron-right"></i>
            </button>
        </form>
    );
}