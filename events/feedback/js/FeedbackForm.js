"use strict";

const initialState = {
  form: {
    snacks: []
  }
}
class FeedbackForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = initialState;
    this.handleChange = this.handleChange.bind(this);
    this.submitData = this.submitData.bind(this);
    this.onSubmit = this.props.onSubmit.bind(this);
  }

  resetState = () => {
    this.setState(this.getInitialState());
 }

  handleChange(event) {
    const target = event.target,
      name = target.name;
    let value;
    if (
      (target.type === "checkbox" && name !== "snacks") ||
      target.type === "radio"
    ) {
      value = target.checked;
    } else {
      if (name === "salutation") {
        value = name;
      } else if (name === "snacks") {
        value = [...this.state.form.snacks, target.value];
      } else value = target.value;
    }

    this.setState({
      form: {...this.state.form, [name]: value}
    });
  }

  submitData(event) {
    event.preventDefault();
    this.props.onSubmit(JSON.stringify(this.state));
    this.setState(initialState)
  }

  render() {
    return (
      <form className="content__form contact-form" onSubmit={this.submitData}>
        <div className="testing">
          <p>Чем мы можем помочь?</p>
        </div>
        <div className="contact-form__input-group">
          <input
            onChange={this.handleChange}
            className="contact-form__input contact-form__input--radio"
            id="salutation-mr"
            name="salutation"
            type="radio"
            value="Мистер"
          />
          <label
            className="contact-form__label contact-form__label--radio"
            htmlFor="salutation-mr"
          >
            Мистер
          </label>
          <input
            onChange={this.handleChange}
            className="contact-form__input contact-form__input--radio"
            id="salutation-mrs"
            name="salutation"
            type="radio"
            value="Мисис"
          />
          <label
            className="contact-form__label contact-form__label--radio"
            htmlFor="salutation-mrs"
          >
            Мисис
          </label>
          <input
            onChange={this.handleChange}
            className="contact-form__input contact-form__input--radio"
            id="salutation-ms"
            name="salutation"
            type="radio"
            value="Мис"
          />
          <label
            className="contact-form__label contact-form__label--radio"
            htmlFor="salutation-ms"
          >
            Мис
          </label>
        </div>
        <div className="contact-form__input-group">
          <label className="contact-form__label" htmlFor="name">
            Имя
          </label>
          <input
            onChange={this.handleChange}
            className="contact-form__input contact-form__input--text"
            id="name"
            name="name"
            type="text"
          />
        </div>
        <div className="contact-form__input-group">
          <label className="contact-form__label" htmlFor="email">
            Адрес электронной почты
          </label>
          <input
            onChange={this.handleChange}
            className="contact-form__input contact-form__input--email"
            id="email"
            name="email"
            type="email"
          />
        </div>
        <div className="contact-form__input-group">
          <label className="contact-form__label" htmlFor="subject">
            Чем мы можем помочь?
          </label>
          <select
            onChange={this.handleChange}
            className="contact-form__input contact-form__input--select"
            id="subject"
            name="subject"
          >
            <option>У меня проблема</option>
            <option>У меня важный вопрос</option>
          </select>
        </div>
        <div className="contact-form__input-group">
          <label className="contact-form__label" htmlFor="message">
            Ваше сообщение
          </label>
          <textarea
            onChange={this.handleChange}
            className="contact-form__input contact-form__input--textarea"
            id="message"
            name="message"
            rows="6"
            cols="65"
          />
        </div>
        <div className="contact-form__input-group">
          <p className="contact-form__label--checkbox-group">Хочу получить:</p>
          <input
            onChange={this.handleChange}
            className="contact-form__input contact-form__input--checkbox"
            id="snacks-pizza"
            name="snacks"
            type="checkbox"
            value="пицца"
          />
          <label
            className="contact-form__label contact-form__label--checkbox"
            htmlFor="snacks-pizza"
          >
            Пиццу
          </label>
          <input
            onChange={this.handleChange}
            className="contact-form__input contact-form__input--checkbox"
            id="snacks-cake"
            name="snacks"
            type="checkbox"
            value="пирог"
          />
          <label
            className="contact-form__label contact-form__label--checkbox"
            htmlFor="snacks-cake"
          >
            Пирог
          </label>
        </div>
        <button className="contact-form__button" type="submit">
          Отправить сообщение!
        </button>
        <output id="result" />
      </form>
    );
  }
}