"use strict";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: "All"
    };
    this.selectFilter = this.selectFilter.bind(this);
    this.getPickedProjects = this.getPickedProjects.bind(this);
  }
  selectFilter(filter) {
    this.setState({
      selected: filter
    });
  }
  getPickedProjects() {
    if (this.state.selected === "All") {
      return this.props.projects;
    } else {
      return this.props.projects.filter(el => el.category === this.state.selected);
    }
  }
  render() {
    return (
      <div>
        <Toolbar
          filters={this.props.filters}
          selected={"All"}
          onSelectFilter={filter => this.selectFilter(filter)}
        />
        <Portfolio projects={this.getPickedProjects()} />
      </div>
    );
  }
}
