'use strict';

const Menu = props => {
  return (
    <div className={`menu ${props.opened ? 'menu-open' : ''}`}>
      <div className="menu-toggle">
        <span />
      </div>
      {props.opened ? (
        <nav>
          <ul>
            {props.items.map((el, id) => {
              return (<li key={id}>
                <a href={el.href}>{el.title}</a>
              </li>)
            })}
          </ul>
        </nav>
      ) : null}
    </div>
  );
};
