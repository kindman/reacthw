'use strict';

const days = [
    "Понедельник",
    "Вторник",
    "Среда",
    "Четверг",
    "Пятница",
    "Суббота",
    "Воскресенье"
  ],
  shortDays = ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"],
  months = [
    "Январь",
    "Февраль",
    "Март",
    "Апрель",
    "Май",
    "Июнь",
    "Июль",
    "Август",
    "Сентябрь",
    "Октябрь",
    "Ноябрь",
    "Декабрь"
  ],
  monthsDeclension = [
    "Января",
    "Февраля",
    "Марта",
    "Апреля",
    "Мая",
    "Июня",
    "Июля",
    "Августа",
    "Сентября",
    "Октября",
    "Ноября",
    "Декабря"
  ];

let weekNumber = 0;
const cols = [];
let classWeekend;
while (weekNumber !== 7) {
  if (weekNumber > 4) {
    classWeekend = "ui-datepicker-week-end";
  }
  cols.push(<col className={classWeekend} />);
  weekNumber++;
}

function getWeekDay(day) {
  return days[day];
}
function getMonthName(month, declension = false) {
  if (declension) {
    return monthsDeclension[month];
  }
  return months[month];
}

function getMonthArray(date) {
  const monthArray = [];
  const month = date.getMonth();
  const year = date.getFullYear();
  const firstDayWeek = (new Date(year, month, 1)).getDay();
  
  let firstDateNumberIndicated = 1;
  switch (firstDayWeek) {
      case 0:
          firstDateNumberIndicated = -5;
          break;
      case 1:
          firstDateNumberIndicated = 1;
          break;
      case 2:
          firstDateNumberIndicated = 0;
          break;
      case 3:
          firstDateNumberIndicated = -1;
          break;
      case 4:
          firstDateNumberIndicated = -2;
          break;
      case 5:
          firstDateNumberIndicated = -3;
          break;
      case 6:
          firstDateNumberIndicated = -4;
          break;
  }

  const lastDateNumber = (new Date(year, month + 1, 0)).getDate();
  const weeksAmount = Math.ceil( (lastDateNumber - firstDateNumberIndicated + 1) / 7 );
  
  let currentDateNumber = firstDateNumberIndicated;
  for (let i = 0; i < weeksAmount; i++) {
      const weekArray = [];
      for (let j = 0; j < 7; j++) {
          weekArray.push( ( new Date(year, month, currentDateNumber) ).getDate() );
          currentDateNumber++;
      }
      monthArray.push(weekArray);
  }

  return monthArray;
}

const TableBody = ({date}) => {
  const today = date.getDate();
  
  function getWeekTR(week, weekIndex) {
    function getDayTD(dayNumber) {
      if (weekIndex === 0 && dayNumber > 20) {
        return <td className="ui-datepicker-other-month">{dayNumber}</td>;
      }

      if (weekIndex > 3 && dayNumber < 10) {
        return <td className="ui-datepicker-other-month">{dayNumber}</td>;
      }

      if (dayNumber === today) {
        return <td className="ui-datepicker-today">{dayNumber}</td>;
      }

      return <td>{dayNumber}</td>;
    }

    return <tr>{week.map(getDayTD)}</tr>;
  }

  return <tbody>{getMonthArray(date).map(getWeekTR)}</tbody>;
};

const Calendar = ({date}) => {
  return (
    <div className="ui-datepicker">
      <div className="ui-datepicker-material-header">
        <div className="ui-datepicker-material-day">
          {getWeekDay(date.getDay())}
        </div>
        <div className="ui-datepicker-material-date">
          <div className="ui-datepicker-material-day-num">{date.getDate()}</div>
          <div className="ui-datepicker-material-month">
            {getMonthName(date.getMonth(), true)}
          </div>
          <div className="ui-datepicker-material-year">{date.getFullYear()}</div>
        </div>
      </div>
      <div className="ui-datepicker-header">
        <div className="ui-datepicker-title">
          <span className="ui-datepicker-month">
            {getMonthName(date.getMonth())}
          </span>
          &nbsp;
          <span className="ui-datepicker-year">{date.getFullYear()}</span>
        </div>
      </div>
      <table className="ui-datepicker-calendar">
        <colgroup>{cols}</colgroup>
        <thead>
          <tr>
            {days.map((el, id) => (
              <th key={id} scope="col" title={el}>
                {shortDays[id]}
              </th>
            ))}
          </tr>
        </thead>
        <TableBody date={date} />
      </table>
    </div>
  );
};
